<?php

class PriorityController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$priority = Priority::all();
		return View::make('admin/priority/priority', array('priorities' => $priority));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//This should be used if you want to have a seperate page for the add priority form
		//uri /priority/create
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array('label' => 'required');
		$validation = Validator::make(Input::all(), $rules);
		if($validation->fails())
		{
			return Redirect::to('priority')->withErrors($validation)->withInput();
		}
		$priority = new Priority;
		$priority->label = Input::get('label'); 
		$priority->description = Input::get('description'); 
		
		//Later, get user id and populate it.  Hard coding for now.
		$priority->created_by = 1;
		$priority->updated_by = 1;
				
		$priority->save();
		return Redirect::to('priority')->with('status', 'Add Successful');
		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{	$priority = Priority::find($id);		
		$view =  View::make('admin/priority/priority_edit');
		$view->id = $priority->id;
		$view->label = $priority->label;
		$view->description = $priority->description;
		return $view;
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = array('label' => 'required');
		$validation = Validator::make(Input::all(), $rules);
		if($validation->fails())
		{
			return Redirect::to('priority.edit')->withErrors($validation)->withInput();
		}
		
		$priority = Priority::find($id);
		$priority->label = Input::get('label');
		$priority->description = Input::get('description');
		$priority->save();
		return Redirect::to('priority')->with('status', 'Update Successful');
		
		
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$priority = Priority::find($id);
		$priority->delete();
		return Redirect::to('priority')->with('status', 'Delete Successful');
	}

}