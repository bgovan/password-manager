<?php

/**
 * 
 * @author tjbryant
 *
 */
class AuthenticationController extends \BaseController {

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('login/login');
	}

	/**
	 * This method will perform the following actions:<br>
	 * 
	 * <ol>
	 * 	<li>validate the form input</li>
	 * 	<li>try to authenticate to ldap</li>
	 * 	<li>try to authenticate to database if ldap fails</li>
	 * 	<li>if either authentication is successful, redirect to intended page</li>
	 * 	<li>if authentication is not successfull, display an error message</li>
	 * </ol>
	 *
	 * @return Response
	 */
	public function authenticate()
	{
		$rules = array('username' => 'required',
				'password' => 'required');
		
		$validation = Validator::make(Input::all(), $rules);
		
		if($validation->fails())
		{
			return Redirect::to('login/login')->withErrors($validation)->withInput();
		}
				
		$user = array(
			'username' => Input::get('username'),
			'password' => Input::get('password')
		);
				
		Log::debug('attempting ldap authentication');
		if($this->ldapAuthentication($user))
		{
			if(Request::path() != 'login')
			{
				return Redirect::intended('/');
			}
			else
			{
				return Redirect::to('/');
			}
		}
		
		Log::debug('attempting database authentication');
		if(Auth::attempt($user))
		{	
			if(Request::path() != 'login')
			{
				return Redirect::intended('/');
			}
			else
			{
				return Redirect::to('/');
			}
		}
		
		Log::debug('user could not be authenticated');
		return Redirect::to('login')->with('login_error', 'Invalid login');
	}
	
	/**
	 * 
	 * @param unknown $user
	 * @return Ambigous <boolean>
	 */
	private function ldapAuthentication($user)
	{
		$result = Ldap_library::authenticateUser($user['username'], $user['password']);
		if($result['authenticated'])
		{
			$this->addUser($user);
		}
		return $result['authenticated'];

	}
	
	/**
	 * This method will save an ldap user in the database if it does not already exist.
	 * 
	 * @param unknown $user
	 */
	private function addUser($user)
	{
		$existingCount = User::where('username', '=', $user['username'])->count();
		
		Log::debug('count: ' . $existingCount);
		
		if($existingCount == 0)
		{
			Log::debug('adding user');
			$newUser = new User;
			$newUser->username = $user['username'];
			$newUser->email = $user['username'] . Config::get('organization.email');
			$newUser->user_types_id = Config::get('app.default.user.type');
			$newUser->created_by = 1;
			$newUser->updated_by = 1;
			$newUser->save();
		}
	}


}