<?php

///////if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @package    utilities
* @access      Public
* @author      tjbryant
*/

class Ldap_library
{
	/**
	 * Returns an array with the ldap connection properties.<br>
	 * 
	 * @return multitype:string multitype:string
	 */
	private static function getLdapConfig(){
		$config = array();
		$config['filter'] = Config::get('auth.ldap.filter');
		$config['attributes'] =Config::get('auth.ldap.attributes');
		$config['protocol'] = Config::get('auth.ldap.protocol');
		$config['host'] = Config::get('auth.ldap.host');
		$config['basedn'] = Config::get('auth.ldap.basedn');
		$config['port'] = Config::get('auth.ldap.port');
		return $config;
	}

	/**
	 * Authenicate LDAP user against Active Directory repository <br>
	 * The return array will have a boolean 'authenticated' property as well as ldap properties.<br>
	 * 
	 * @param unknown $username
	 * @param unknown $password
	 * @return multitype:|Ambigous <multitype:boolean , multitype:>
	 */
	public static function authenticateUser($username, $password)
	{
		Log::info("Entering: Ldap_library::authenticateUser");
		$result = array('authenticated'=>FALSE);
		$attributes = array("Sn", "Cn", "title", "telephoneNumber", "extensionAttribute14", "extensionAttribute15");
		$config = Ldap_library::getLdapConfig();
		$connection =  Ldap_library::createLdapConnection($config['protocol'],$config['host'],$config['port']);
		$bind = Ldap_library::bindLdapProxyUser("CN=$username,{$config['basedn']}",$password, $config, $connection);
		if ($bind) 
		{	
			$ldap_user = ldap_search($connection, $config['basedn'], $config['filter'] . "=" . strtolower(trim($username)), $attributes);
			$ldap_user_entry = ldap_first_entry($connection,$ldap_user);
			$attributes = ldap_get_attributes($connection,$ldap_user_entry);
			$attributes = Ldap_library::extractAttributes($attributes);
			$result['authenticated']=TRUE;		
			$result = array_merge($result, $attributes);
			Log::info("Exiting: Ldap_library::authenticateUser");
			return $result;
		}
		Log::info("Exiting: Ldap_library::authenticateUser");
		return $result;
	}

	
	/**
	 * Cleans up the attributes array.<br>
	 * 
	 * @param unknown $attributes
	 * @return multitype:unknown
	 */
	public static function extractAttributes($attributes)
	{
		
		$cleaned_atts = array(); // the returned attributes are a little convoluted
		if (!empty($attributes)) {
			foreach($attributes as $att_key => $att_val) {
				if ($att_key == 'count' || is_numeric($att_key)) {
					continue;
				}
				$cleaned_atts[$att_key] = $att_val[0];
			}
		}
		return $cleaned_atts;
	}

	/**
	 * Create Initial LDAP connection to the AD Server<br>
	 *
	 * @param string $protocol Connection Protocol
	 * @param string $host LDAP Host Name
	 * @param int $port Connection Port
	 * @return mixed connection object or boolean (false)
	 * @access Private
	 */
	private static function createLdapConnection($protocol, $host, $port)
	{
		$ldap_connection = ldap_connect($protocol . "://" . $host . ":" . $port);
		ldap_set_option($ldap_connection, LDAP_OPT_PROTOCOL_VERSION, 3);
		ldap_set_option($ldap_connection, LDAP_OPT_REFERRALS, 0);
		if (!$ldap_connection) {
			Log::error('ERROR: Failed to create ldap connection.');
			return FALSE;
		}
		Log::debug('DEBUG: LDAP connection created successfully.');
		return $ldap_connection;
	}

	/**
	 * Bind proxy user to LDAP connection. This is done so the user's password isn't sent over then network.<br>
	 *
	 * @param object $ldap_connection Connection Object
	 * @param string $proxy_user Proxy user name
	 * @param string $proxy_pass Proxy Password
	 * @param array $config Ldap Configuration
	 * @param connection $connection ldap connection
	 * @return bool TRUE/FALSE
	 * @access Private
	 */
	private static function bindLdapProxyUser($proxy_user, $proxy_pass, $config, $connection)
	{	
		$proxy_bind = @ldap_bind($connection,$proxy_user,$proxy_pass);
		if (!$proxy_bind) {
			Log::error('ERROR: Failed to bind to ldap proxy user.');
			return FALSE;
		}
		Log::debug('DEBUG: Proxy user bind successful.');
		return TRUE;
	}

}

?>
