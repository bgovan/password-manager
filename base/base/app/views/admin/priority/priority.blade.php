@extends('/layouts/admin')
@section('content')
	<h2>Priority</h2>
	<div>
		<h3>Add Priority</h3>
		<?php 
			if(Session::has('status'))
			{
				echo Session::get('status');
				Session::remove('status');
			}			
		?>
		
		<?php 
			$messsages = $errors->all('<p style="color:red">:message</p>') 
		?>
		<?php 
			foreach($messsages as $msg)
			{
				echo $msg;
			}			
		?>
		
		<?= Form::open() ?>
		<?= Form::label('label', 'Label') ?>
		<?= Form::text('label') ?>
		<?= Form::label('description','Description') ?>
		<?= Form::textarea('description') ?>
		<?= Form::submit('Add Priority') ?>
		<?= Form::close() ?>
	</div>
	<div>
		<h3>Existing Priorities</h3>
		<table>
			<tr>
				<th>label</th>
				<th>description</th>
				<th>action</th>
			</tr>
			@foreach($priorities as $priority)
				<tr>
					<td>{{ $priority->label }}</td>
					<td>{{ $priority->description }}</td>
					<td>	
						{{ Form::open(array('route' => array('priority.destroy', $priority->id), 'method' => 'delete')) }}
							<button type="submit" href="{{ URL::route('priority.destroy', $priority->id) }}">Delete</button>
						{{ Form::close() }}
						{{ Form::open(array('route' => array('priority.edit', $priority->id), 'method' => 'get')) }}
							<button type="submit" href="{{ URL::route('priority.edit', $priority->id) }}">Update</button>
						{{ Form::close() }}
					</td>
				</tr>
			@endforeach
		</table>
	</div>
@stop