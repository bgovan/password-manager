@extends('/layouts/layout')
@section('content')
	<div>
		<h3>Edit Priority</h3>
		
		<?php 
			if(Session::has('status'))
			{
				echo Session::get('status');
				Session::remove('status');
			}			
		?>
		
		<?php 
			$messsages = $errors->all('<p style="color:red">:message</p>') 
		?>
		<?php 
			foreach($messsages as $msg)
			{
				echo $msg;
			}			
		?>
		
		<?= Form::open(array('route' => array('priority.update', $id), 'method' => 'put')) ?>
		<?= Form::label('label', 'Label') ?>
		<?= Form::text('label', $label) ?>
		<?= Form::label('description','Description') ?>
		<?= Form::textarea('description', $description) ?>
		<?= Form::submit('Update Priority') ?>
		<?= Form::close() ?>
	</div>
@stop