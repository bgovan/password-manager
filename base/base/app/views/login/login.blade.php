@extends('/layouts/layout')
@section('content')
	<h2>Welcome</h2>

	<div>
		<?php 
			$messsages = $errors->all('<p style="color:red">:message</p>') 
		?>
		<?php 
			foreach($messsages as $msg)
			{
				echo $msg;
			}			
		?>
		
		<?php 
			if(Session::has('login_error'))
			{
				echo Session::get('login_error');
				Session::remove('login_error');
			}			
		?>
		
		<?= Form::open() ?>
		<?= Form::label('username', 'User Name: ') ?>
		<?= Form::text('username', Input::old('username')) ?>
		<br>
		<?= Form::label('password', 'Password:') ?>
		<?= Form::password('password') ?>
		<br>
		<?= Form::submit('Login') ?>
		<?= Form::close() ?>
		
		
	</div>
	
	
@stop