<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrioritiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('priorities', function($table){
			$table->increments('id');
			$table->string('label');
			$table->string('description')->nullable();
			$table->integer('rank');
			//name, auto increment, unsigned
			$table->integer('created_by');
			$table->integer('updated_by');
			$table->timestamps();
			$table->softDeletes();
		});
		
		DB::table('priorities')->insert(
			array(
				array('label' => 'Low', 'created_by' => 1,'updated_by' => 1, 'rank' => 1,
					'description' => 'Low priority means 1 or more users are inconvenienced by the issue but can continue working.' ),
				array('label' => 'Medium', 'created_by' => 1,'updated_by' => 1,'rank' => 2,
					'description' => 'Medium priority means 1 user is unable to work due to the issue.'),
				array('label' => 'High', 'created_by' => 1,'updated_by' => 1,'rank' => 3,
					'description' => 'Medium priority multiple users are unable to work due to the issue.'),
			)
		);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('priorities');
	}

}
