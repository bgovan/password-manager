<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function($table)
		{
			$table->increments('id');
			$table->string('username');
			$table->string('email');
			$table->string('password', 64)->nullable();
			$table->string('first_name')->nullable();
			$table->string('last_name')->nullable();
			$table->integer('user_types_id');
			$table->integer('created_by');
			$table->integer('updated_by');
			$table->timestamps();
			$table->softDeletes();
		});
		
		DB::table('users')->insert(
			array(
				array('username' => 'admin', 'email' => 'admin@uncc.edu', 'password' => Hash::make('R@1ning@Ga!n'),
				'user_types_id' => 1, 'first_name' => 'Admin', 'last_name' => 'Admin', 
				'created_by' => 1, 'updated_by' => 1)
			)
		);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		drop('users');
	}

}
