<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateUserTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_types', function($table){
			$table->increments('id');
			$table->string('label');
			$table->string('description')->nullable();
			$table->integer('created_by');
			$table->integer('updated_by');
			$table->timestamps();
			$table->softDeletes();
		});
		
		DB::table('user_types')->insert(
			array(
			array('label' => 'Admin', 'created_by' => 1,'updated_by' => 1 ),
			array('label' => 'Helpdesk', 'created_by' => 1,'updated_by' => 1),
			array('label' => 'Faculty', 'created_by' => 1,'updated_by' => 1),
			array('label' => 'User', 'created_by' => 1,'updated_by' => 1),
			)
		);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_types');
	}

}
