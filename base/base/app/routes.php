<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('index');
});

Route::group(array('before'=>'auth'), function(){
	Route::resource('priority', 'PriorityController');
});


Route::group(array('before'=>'guest'), function(){
	Route::get('login', 'AuthenticationController@index');
	Route::post('login', 'AuthenticationController@authenticate');
});
