<?php

class UserTest extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function testUserTypeLabel()
	{
		$user = User::find(1);
		$this->assertTrue($user->id == 1);
		$this->assertTrue($user->getAttribute('user_types_id') == 1);
		$this->assertTrue($user->userTypeLabel() == 'Admin');
	}

}